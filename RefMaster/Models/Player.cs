﻿using System;
namespace RefMaster.Models
{
    public class Player
    {
        public int number { get; set; }
        public string name { get; set; }

        // Disable default constructor
        public Player() { }

        public Player(int number, string playerName = null)
        {
            this.number = number;
            this.name = ((playerName == null || playerName == "") ? null : playerName);
        }

        public string GetName()
        {
            return this.name;
        }

        public int GetNumber()
        {
            return this.number;
        }

        // Debug
        public void Print()
        {
            Console.WriteLine("I am a player: " + this.name + ":" + this.number);
        }
    }
}
