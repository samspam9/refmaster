﻿using System;
using System.Collections.Generic;
using RefMaster.Pages;

namespace RefMaster.Models
{
    public class MatchData
    {
        public MatchData(Team t1, Team t2, List<Set> sets, int sct1, int sct2, int set1, int set2)
        {
            team1 = t1;
            team2 = t2;
            this.sets = sets;
            team1Sanctions = sct1;
            team2Sanctions = sct2;
            id = null;
            team1Sets = set1;
            team2Sets = set2;
        }

        public Team team1 { get; set; }
        public Team team2 { get; set; }
        public List<Set> sets { get; set; }
        public int team1Sanctions { get; set; }
        public int team2Sanctions { get; set; }
        public int team1Sets { get; set; }
        public int team2Sets { get; set; }
        public string id { get; set; }
    }
}
