﻿using System;
using Xamarin.Forms;

namespace RefMaster.Models
{
    public class HistoryEntry
    {
        public HistoryEntry(int id, string name1, string name2, string score, Color co1, Color co2)
        {
            this.Id = id;
            this.ScoreString = score;
            this.Team1Name = name1;
            this.Team2Name = name2;
            this.Team1Color = co1;
            this.Team2Color = co2;
        }

        public int Id { get; set; }
        public string Team1Name { get; set; }
        public string Team2Name { get; set; }
        public Color Team1Color { get; set; }
        public Color Team2Color { get; set; }
        public string ScoreString { get; set; }
    }
}
