﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Xamarin.Forms;

namespace RefMaster.Models
{
    public class ColorHolder
    {
        public string Value { get; set; }
        [JsonIgnore]
        public Xamarin.Forms.Color color { get; set; }
    }

    public class Team
    {
        // Contain 5 players (all the team except for the captain)
        public List<Player> players { get; set; }
        public Player captain { get; set; }
        public string name { get; set; }
        public ColorHolder teamColor { get; set; }

        public Team() { }

        public Team(string name, Color color)
        {
            this.name = name;
            this.teamColor = new ColorHolder() { Value = color.ToHex() };
            this.players = new List<Player>();
        }

        public void SetCaptain(Player captain)
        {
            this.captain = captain;
        }

        public void AddPlayer(Player member)
        {
            players.Add(member);
        }

        public string GetName()
        {
            return this.name;
        }

        public Player GetCaptain()
        {
            return this.captain;
        }

        public List<Player> GetPlayers()
        {
            return this.players;
        }

        public Color GetColor()
        {
            return  Color.FromHex(this.teamColor.Value);
        }

        // Debug
        public void Print()
        {
            Console.WriteLine("My team: " + this.name);
            captain.Print();
            foreach (Player pl in players)
            {
                pl.Print();
            }
            Console.WriteLine("END TEAM " + this.name);
        }
    }
}
