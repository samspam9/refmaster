﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using RefMaster.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RefMaster.Pages
{
    public class Set
    {
        public int team1;
        public int team2;
    }
    public class Game
    {
        public int t1Pts = 0;
        public int t2Pts = 0;
        public int t1Sets = 0;
        public int t2Sets = 0;
        public int t1Scts = 0;
        public int t2Scts = 0;
        public List<Set> sets = new List<Set>();
        public List<Team> teams;
    }

    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MatchPage : ContentPage
    {
        Ruleset ruleset;
        Game game;
        public MatchPage(Ruleset configRuleset, List<Team> teams)
        {
            InitializeComponent();
            ruleset = configRuleset;
            game = new Game();
            game.teams = teams;
            team1Frame.BorderColor = game.teams[0].GetColor();
            team2Frame.BorderColor = game.teams[1].GetColor();
            team1Name.Text = game.teams[0].GetName();
            team2Name.Text = game.teams[1].GetName();
            team1Points.BackgroundColor = game.teams[0].GetColor();
            team2Points.BackgroundColor = game.teams[1].GetColor();
            team1SanctionsButton.BackgroundColor = game.teams[0].GetColor();
            team2SanctionsButton.BackgroundColor = game.teams[1].GetColor();
            team1p1.Text = game.teams[0].GetCaptain().GetNumber().ToString();
            team1p2.Text = game.teams[0].GetPlayers()[0].GetNumber().ToString();
            team1p3.Text = game.teams[0].GetPlayers()[1].GetNumber().ToString();
            team1p4.Text = game.teams[0].GetPlayers()[2].GetNumber().ToString();
            team1p5.Text = game.teams[0].GetPlayers()[3].GetNumber().ToString();
            team1p6.Text = game.teams[0].GetPlayers()[4].GetNumber().ToString();
            team2p1.Text = game.teams[1].GetCaptain().GetNumber().ToString();
            team2p2.Text = game.teams[1].GetPlayers()[0].GetNumber().ToString();
            team2p3.Text = game.teams[1].GetPlayers()[1].GetNumber().ToString();
            team2p4.Text = game.teams[1].GetPlayers()[2].GetNumber().ToString();
            team2p5.Text = game.teams[1].GetPlayers()[3].GetNumber().ToString();
            team2p6.Text = game.teams[1].GetPlayers()[4].GetNumber().ToString();
        }

        // Return Home Button
        private void Home_Clicked(object sender, EventArgs e)
        {
            Navigation.PopToRootAsync();
        }

        // Go Back one page
        private void Back_Clicked(object sender, EventArgs e)
        {
            Navigation.PopAsync(true);
        }

        private void team1Points_Clicked(object sender, EventArgs e)
        {
            game.t1Pts += 1;
            team1Points.Text = game.t1Pts.ToString();
            team2Service.Opacity = 0;
            team1Service.Opacity = 1;
            if (game.t1Pts == ruleset.pointsPerSet)
            {
                game.t1Sets += 1;
                team1Sets.Text = game.t1Sets.ToString();
                if (game.t1Sets == 3 && ruleset.setsPerMatch == 5 && ruleset.matchEnds == "teamwins")
                {
                    game.sets.Add(new Set() { team1 = game.t1Pts, team2 = game.t2Pts });
                    DisplayAlert("Game over", game.teams[0].GetName() + " wins", "OK");
                    EndOfMatch();

                }
                else if (game.t1Sets == 2 && ruleset.setsPerMatch == 3 && ruleset.matchEnds == "teamwins")
                {
                    game.sets.Add(new Set() { team1 = game.t1Pts, team2 = game.t2Pts });
                    DisplayAlert("Game over", game.teams[0].GetName() + " wins", "OK");
                    EndOfMatch();
                }
                else if (game.t1Sets == 1 && ruleset.setsPerMatch == 1 && ruleset.matchEnds == "teamwins")
                {
                    game.sets.Add(new Set() { team1 = game.t1Pts, team2 = game.t2Pts });
                    DisplayAlert("Game over", game.teams[0].GetName() + " wins", "OK");
                    EndOfMatch();
                }
                else if (game.t1Sets == ruleset.setsPerMatch && ruleset.matchEnds == "allsetsover")
                {
                    game.sets.Add(new Set() { team1 = game.t1Pts, team2 = game.t2Pts });
                    DisplayAlert("Game over", game.teams[0].GetName() + " wins", "OK");
                    EndOfMatch();
                }
                else
                    goToNextSet("team1");
            }
        }

        private void team2Points_Clicked(object sender, EventArgs e)
        {
            game.t2Pts += 1;
            team2Points.Text = game.t2Pts.ToString();
            team1Service.Opacity = 0;
            team2Service.Opacity = 1;
            if (game.t2Pts == ruleset.pointsPerSet)
            {
                game.t2Sets += 1;
                team2Sets.Text = game.t2Sets.ToString();
                if (game.t2Sets == 3 && ruleset.setsPerMatch == 5 && ruleset.matchEnds == "teamwins")
                {
                    game.sets.Add(new Set() { team1 = game.t1Pts, team2 = game.t2Pts });
                    DisplayAlert("Game over", game.teams[1].GetName() + " wins", "OK");
                    EndOfMatch();
                }
                else if (game.t2Sets == 2 && ruleset.setsPerMatch == 3 && ruleset.matchEnds == "teamwins")
                {
                    game.sets.Add(new Set() { team1 = game.t1Pts, team2 = game.t2Pts });
                    DisplayAlert("Game over", game.teams[1].GetName() + " wins", "OK");
                    EndOfMatch();
                }
                else if (game.t2Sets == 1 && ruleset.setsPerMatch == 1 && ruleset.matchEnds == "teamwins")
                {
                    game.sets.Add(new Set() { team1 = game.t1Pts, team2 = game.t2Pts });
                    DisplayAlert("Game over", game.teams[1].GetName() + " wins", "OK");
                    EndOfMatch();
                }
                else if (game.t2Sets == ruleset.setsPerMatch && ruleset.matchEnds == "allsetsover")
                {
                    game.sets.Add(new Set() { team1 = game.t1Pts, team2 = game.t2Pts });
                    DisplayAlert("Game over", game.teams[1].GetName() + " wins", "OK");
                    EndOfMatch();
                }
                else
                    goToNextSet("team2");
            }
        }

        private void goToNextSet(string winner)
        {
            game.sets.Add(new Set() { team1 = game.t1Pts, team2 = game.t2Pts }) ;
            game.t1Pts = 0;
            team1Points.Text = "0";
            game.t2Pts = 0;
            team2Points.Text = "0";
            if (winner == "team1")
            {
                team2Service.Opacity = 1;
                team1Service.Opacity = 0;
            }
            else
            {
                team2Service.Opacity = 0;
                team1Service.Opacity = 1;
            }
        }

        private void team1Sanctions_Clicked(object sender, EventArgs e)
        {
            game.t1Scts += 1;
            team1Sanctions.Text = game.t1Scts.ToString();
            if (game.t1Scts == 2)
            {
                game.sets.Add(new Set() { team1 = game.t1Pts, team2 = game.t2Pts });
                DisplayAlert("Game over", game.teams[1].GetName() + " wins", "OK");
                EndOfMatch();
            }
        }

        private void team2Sanctions_Clicked(object sender, EventArgs e)
        {
            game.t2Scts += 1;
            team2Sanctions.Text = game.t2Scts.ToString();
            if (game.t2Scts == 2)
            {
                game.sets.Add(new Set() { team1 = game.t1Pts, team2 = game.t2Pts });
                DisplayAlert("Game over", game.teams[0].GetName() + " wins", "OK");
                EndOfMatch();
            }
        }

        private static HttpClient httpClient = new HttpClient();

        private async void EndOfMatch()
        {
            MatchData match = new MatchData(game.teams[0], game.teams[1], game.sets, game.t1Scts, game.t2Scts, game.t1Sets, game.t2Sets);
            string output = JsonConvert.SerializeObject(match, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Include, Formatting = Formatting.Indented, TypeNameHandling = TypeNameHandling.Auto });
            var request = new HttpRequestMessage();

            request.Method = HttpMethod.Post;
            request.RequestUri = new Uri("https://refmaster.uc.r.appspot.com/history");
            request.Content = new StringContent(output, Encoding.UTF8, "application/json");
            try
            {
                await httpClient.SendAsync(request);
            } catch (Exception) {}
            await Navigation.PopToRootAsync();
        }
    }
}