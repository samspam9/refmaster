﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RefMaster.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RefMaster.Pages
{
    public class Ruleset
    {
        public string name { get; set; }
        public int setsPerMatch { get; set; }
        public int pointsPerSet { get; set; }
        public bool tieBreak { get; set; }
        public int tieBreakPoints { get; set; }
        public bool sanctions { get; set; }
        public string matchEnds { get; set; }
        public bool teamTO { get; set; }
        public int teamTONb { get; set; }
        public int teamTODur { get; set; }
        public bool techTO { get; set; }
        public int techTODur { get; set; }
        public bool breaks { get; set; }
        public int breaksDur { get; set; }   
    }
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MatchConfigPage : ContentPage
    {
        private Ruleset ruleset = new Ruleset {name = "FIVB indoor 6x6 rules", setsPerMatch = 5, pointsPerSet = 25,
            tieBreak = true, tieBreakPoints = 25, matchEnds = "teamwins", teamTO = true, teamTONb = 3, teamTODur = 60,
            techTO = true, techTODur = 60, breaks = true, breaksDur = 180};
        private List<Team> teams;
        public MatchConfigPage(List<Team> teams)
        {
            InitializeComponent();
            this.teams = teams;
        }

        // Go To Match Page Button
        private void Next_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new MatchPage(ruleset, teams));
        }

        // Go Back one page
        private void Back_Clicked(object sender, EventArgs e)
        {
            Navigation.PopAsync(true);
        }

        // Event fired when the sets per match picker is selected
        private void SPM_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (spm.SelectedIndex)
            {
                case 0:
                    this.ruleset.setsPerMatch = 5;
                    break;
                case 1:
                    this.ruleset.setsPerMatch = 3;
                    break;
                case 2:
                    this.ruleset.setsPerMatch = 1;
                    break;
            }

        }
        // Event fired when the points per set entry is completed
        private async void PPS_Completed(object sender, EventArgs e)
        {
            var text = ((Entry)sender).Text; //cast sender to access the properties of the Entry
            int number = int.Parse(text);
            if (number >= 9 && number <= 40)
                this.ruleset.pointsPerSet = number;
            else
            {
                await DisplayAlert("Invalid entry", "Points per set have to be between 9 and 40", "OK");
                pointsPerSet.Text = "25";
            }
        }

        private void tieBreak_OnChanged(object sender, ToggledEventArgs e)
        {
            if (tieBreak.On == false)
                tieBreakPoints.TextColor = Color.Gray;
            else
                tieBreakPoints.TextColor = Color.Black;
            tieBreakPoints.IsReadOnly = !tieBreak.On;
            this.ruleset.tieBreak = tieBreak.On;
        }

        private async void TBP_Completed(object sender, EventArgs e)
        {
            var text = ((Entry)sender).Text; //cast sender to access the properties of the Entry
            int number = int.Parse(text);
            if (number >= 9 && number <= 40)
                this.ruleset.tieBreakPoints = number;
            else
            {
                await DisplayAlert("Invalid entry", "Tie break points have to be between 9 and 40", "OK");
                tieBreakPoints.Text = "25";
            }
        }

        private void Sanctions_OnChanged(object sender, ToggledEventArgs e)
        {
            this.ruleset.sanctions = sanctions.On;
        }

        private void matchEnds_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (matchEnds.SelectedIndex == 0)
                this.ruleset.matchEnds = "teamwins";
            else
                this.ruleset.matchEnds = "allsetsover";
        }

        private void teamTO_OnChanged(object sender, ToggledEventArgs e)
        {
            if (teamTO.On == false)
            {
                teamTONb.TextColor = Color.Gray;
                teamTODur.TextColor = Color.Gray;
            }
            else
            {
                teamTONb.TextColor = Color.Black;
                teamTODur.TextColor = Color.Black;
            }
            teamTONb.IsEnabled = teamTO.On;
            teamTODur.IsEnabled = teamTO.On;
            this.ruleset.teamTO = teamTO.On;
        }

        private void teamTONb_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (teamTONb.SelectedIndex)
            {
                case 0:
                    this.ruleset.teamTONb = 3;
                    break;
                case 1:
                    this.ruleset.teamTONb = 2;
                    break;
                case 2:
                    this.ruleset.teamTONb = 1;
                    break;
            }
        }

        private void teamTODur_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (teamTODur.SelectedIndex)
            {
                case 0:
                    this.ruleset.teamTODur = 60;
                    break;
                case 1:
                    this.ruleset.teamTODur = 45;
                    break;
                case 2:
                    this.ruleset.teamTODur = 30;
                    break;
            }
        }

        private void techTO_OnChanged(object sender, ToggledEventArgs e)
        {
            if (techTO.On == false)
                techTODur.TextColor = Color.Gray;
            else
                techTODur.TextColor = Color.Black;
            techTODur.IsEnabled = techTO.On;
            this.ruleset.techTO = techTO.On;
        }

        private void techTODur_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (techTODur.SelectedIndex)
            {
                case 0:
                    this.ruleset.techTODur = 60;
                    break;
                case 1:
                    this.ruleset.techTODur = 45;
                    break;
                case 2:
                    this.ruleset.techTODur = 30;
                    break;
            }
        }

        private void breaks_OnChanged(object sender, ToggledEventArgs e)
        {
            if (breaks.On == false)
                breaksDur.TextColor = Color.Gray;
            else
                breaksDur.TextColor = Color.Black;
            breaksDur.IsEnabled = breaks.On;
            this.ruleset.breaks = breaks.On;
        }

        private void breaksDur_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (breaksDur.SelectedIndex)
            {
                case 0:
                    this.ruleset.breaksDur = 60;
                    break;
                case 1:
                    this.ruleset.breaksDur = 45;
                    break;
                case 2:
                    this.ruleset.breaksDur = 30;
                    break;
            }
        }
    }
}