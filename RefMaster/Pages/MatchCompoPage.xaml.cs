﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RefMaster.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RefMaster.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MatchCompoPage : ContentPage
    {
        public MatchCompoPage()
        {
            InitializeComponent();
        }

        private bool CheckCorrectFields()
        {
            bool anErrorOccured = false;

            // The if forest is mandatory because with an OR, it wouldn't execute the second clause if the first one is false.
            if (CheckCorrectTeamNames() == false)
                anErrorOccured = true;
            if (CheckCorrectTeamColors() == false)
                anErrorOccured = true;
            if (CheckCorrectTeamPlayers() == false)
                anErrorOccured = true;
            return !(anErrorOccured);
        }

        ////////////////////
        // Button clicked //
        ////////////////////
        // Go to match config page Button
        private void Next_Clicked(object sender, EventArgs e)
        {
            if (!CheckCorrectFields())
            {
                return;
            }
            Team team1 = new Team(Team1Name.Text, Team1Color.Color);
            Team team2 = new Team(Team2Name.Text, Team2Color.Color);
            team1.SetCaptain(new Player(int.Parse(T1CaptainNumber.Text), T1CaptainName.Text));
            team2.SetCaptain(new Player(int.Parse(T2CaptainNumber.Text), T2CaptainName.Text));
            team1.AddPlayer(new Player(int.Parse(T1P2Number.Text), T1P2Name.Text));
            team1.AddPlayer(new Player(int.Parse(T1P3Number.Text), T1P3Name.Text));
            team1.AddPlayer(new Player(int.Parse(T1P4Number.Text), T1P4Name.Text));
            team1.AddPlayer(new Player(int.Parse(T1P5Number.Text), T1P5Name.Text));
            team1.AddPlayer(new Player(int.Parse(T1P6Number.Text), T1P6Name.Text));
            team2.AddPlayer(new Player(int.Parse(T2P2Number.Text), T2P2Name.Text));
            team2.AddPlayer(new Player(int.Parse(T2P3Number.Text), T2P3Name.Text));
            team2.AddPlayer(new Player(int.Parse(T2P4Number.Text), T2P4Name.Text));
            team2.AddPlayer(new Player(int.Parse(T2P5Number.Text), T2P5Name.Text));
            team2.AddPlayer(new Player(int.Parse(T2P6Number.Text), T2P6Name.Text));
            List<Team> teams = new List<Team>();
            teams.Add(team1);
            teams.Add(team2);
            Navigation.PushAsync(new MatchConfigPage(teams));
        }

        // Go Back one page
        private void Back_Clicked(object sender, EventArgs e)
        {
            Navigation.PopAsync(true);
        }

        ////////////////////////
        // Text Entry changed //
        ////////////////////////
        void TeamName_TextChanged(System.Object sender, Xamarin.Forms.TextChangedEventArgs e)
        {
            CheckCorrectTeamNames();
        }

        void TeamColor_PropertyChanged(System.Object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            CheckCorrectTeamColors();
        }

        void Number_TextChanged(System.Object sender, Xamarin.Forms.TextChangedEventArgs e)
        {
            CheckCorrectTeamPlayers();
        }

        private bool CheckCorrectTeamColors()
        {
            Team1ColorLabel.TextColor = Color.Default;
            Team2ColorLabel.TextColor = Color.Default;

            if (Team1Color.Color == Team2Color.Color)
            {
                Team1ColorLabel.TextColor = Color.Red;
                Team2ColorLabel.TextColor = Color.Red;
                return false;
            }
            return true;
        }

        // Check the validity of the team name fields
        private bool CheckCorrectTeamNames()
        {
            bool anErrorOccured = false;

            Team1Name.TextColor = Color.Default;
            Team1NameLabel.TextColor = Color.Default;
            Team1Name.PlaceholderColor = Color.Default;
            Team2Name.TextColor = Color.Default;
            Team2NameLabel.TextColor = Color.Default;
            Team2Name.PlaceholderColor = Color.Default;
            if (Team1Name.Text == null || Team1Name.Text == "")
            {
                Team1Name.TextColor = Color.Red;
                Team1NameLabel.TextColor = Color.Red;
                Team1Name.PlaceholderColor = Color.Red;
                anErrorOccured = true;
            }
            if (Team2Name.Text == null || Team2Name.Text == "")
            {
                Team2Name.TextColor = Color.Red;
                Team2NameLabel.TextColor = Color.Red;
                Team2Name.PlaceholderColor = Color.Red;
                anErrorOccured = true;
            }
            if (Team2Name.Text == Team1Name.Text)
            {
                Team1Name.TextColor = Color.Red;
                Team1NameLabel.TextColor = Color.Red;
                Team2Name.TextColor = Color.Red;
                Team2NameLabel.TextColor = Color.Red;
                Team1Name.PlaceholderColor = Color.Red;
                Team2Name.PlaceholderColor = Color.Red;
                anErrorOccured = true;
            }
            return !anErrorOccured;
        }

        private bool IsPlayerConfigCorrect(Entry player)
        {
            if (player.Text == null || player.Text == "")
            {
                player.TextColor = Color.Red;
                player.PlaceholderColor = Color.Red;
                return false;
            }
            try
            {
                int.Parse(player.Text);
            }
            catch (Exception)
            {
                player.TextColor = Color.Red;
                player.PlaceholderColor = Color.Red;
                return false;
            }
            return true;
        }

        private bool CheckCorrectTeamPlayers()
        {
            bool anErrorOccured = false;

            // Team1 Collision check
            T1CaptainNumber.TextColor = Color.Default;
            T1P2Number.TextColor = Color.Default;
            T1P3Number.TextColor = Color.Default;
            T1P4Number.TextColor = Color.Default;
            T1P5Number.TextColor = Color.Default;
            T1P6Number.TextColor = Color.Default;
            T1CaptainNumber.PlaceholderColor = Color.Default;
            T1P2Number.PlaceholderColor = Color.Default;
            T1P3Number.PlaceholderColor = Color.Default;
            T1P4Number.PlaceholderColor = Color.Default;
            T1P5Number.PlaceholderColor = Color.Default;
            T1P6Number.PlaceholderColor = Color.Default;
            if (T1CaptainNumber.Text == T1P2Number.Text || T1CaptainNumber.Text == T1P3Number.Text
                || T1CaptainNumber.Text == T1P4Number.Text || T1CaptainNumber.Text == T1P5Number.Text
                || T1CaptainNumber.Text == T1P6Number.Text)
            {
                T1CaptainNumber.TextColor = Color.Red;
                anErrorOccured = true;
            }
            if (T1P2Number.Text == T1CaptainNumber.Text || T1P2Number.Text == T1P3Number.Text
                || T1P2Number.Text == T1P4Number.Text || T1P2Number.Text == T1P5Number.Text
                || T1P2Number.Text == T1P6Number.Text)
            {
                T1P2Number.TextColor = Color.Red;
                anErrorOccured = true;
            }
            if (T1P3Number.Text == T1P2Number.Text || T1CaptainNumber.Text == T1P3Number.Text
                || T1P3Number.Text == T1P4Number.Text || T1P3Number.Text == T1P5Number.Text
                || T1P3Number.Text == T1P6Number.Text)
            {
                T1P3Number.TextColor = Color.Red;
                anErrorOccured = true;
            }
            if (T1P4Number.Text == T1P2Number.Text || T1P4Number.Text == T1P3Number.Text
                || T1CaptainNumber.Text == T1P4Number.Text || T1P4Number.Text == T1P5Number.Text
                || T1P4Number.Text == T1P6Number.Text)
            {
                T1P4Number.TextColor = Color.Red;
                anErrorOccured = true;
            }
            if (T1P5Number.Text == T1P2Number.Text || T1P5Number.Text == T1P3Number.Text
                || T1P5Number.Text == T1P4Number.Text || T1CaptainNumber.Text == T1P5Number.Text
                || T1P5Number.Text == T1P6Number.Text)
            {
                T1P5Number.TextColor = Color.Red;
                anErrorOccured = true;
            }
            if (T1P6Number.Text == T1P2Number.Text || T1P6Number.Text == T1P3Number.Text
                || T1P6Number.Text == T1P4Number.Text || T1P6Number.Text == T1P5Number.Text
                || T1CaptainNumber.Text == T1P6Number.Text)
            {
                T1P6Number.TextColor = Color.Red;
                anErrorOccured = true;
            }

            // Team2 Collision check
            T2CaptainNumber.TextColor = Color.Default;
            T2P2Number.TextColor = Color.Default;
            T2P3Number.TextColor = Color.Default;
            T2P4Number.TextColor = Color.Default;
            T2P5Number.TextColor = Color.Default;
            T2P6Number.TextColor = Color.Default;
            if (T2CaptainNumber.Text == T2P2Number.Text || T2CaptainNumber.Text == T2P3Number.Text
                || T2CaptainNumber.Text == T2P4Number.Text || T2CaptainNumber.Text == T2P5Number.Text
                || T2CaptainNumber.Text == T2P6Number.Text)
            {
                T2CaptainNumber.TextColor = Color.Red;
                anErrorOccured = true;
            }
            if (T2P2Number.Text == T2CaptainNumber.Text || T2P2Number.Text == T2P3Number.Text
                || T2P2Number.Text == T2P4Number.Text || T2P2Number.Text == T2P5Number.Text
                || T2P2Number.Text == T2P6Number.Text)
            {
                T2P2Number.TextColor = Color.Red;
                anErrorOccured = true;
            }
            if (T2P3Number.Text == T2P2Number.Text || T2CaptainNumber.Text == T2P3Number.Text
                || T2P3Number.Text == T2P4Number.Text || T2P3Number.Text == T2P5Number.Text
                || T2P3Number.Text == T2P6Number.Text)
            {
                T2P3Number.TextColor = Color.Red;
                anErrorOccured = true;
            }
            if (T2P4Number.Text == T2P2Number.Text || T2P4Number.Text == T2P3Number.Text
                || T2CaptainNumber.Text == T2P4Number.Text || T2P4Number.Text == T2P5Number.Text
                || T2P4Number.Text == T2P6Number.Text)
            {
                T2P4Number.TextColor = Color.Red;
                anErrorOccured = true;
            }
            if (T2P5Number.Text == T2P2Number.Text || T2P5Number.Text == T2P3Number.Text
                || T2P5Number.Text == T2P4Number.Text || T2CaptainNumber.Text == T2P5Number.Text
                || T2P5Number.Text == T2P6Number.Text)
            {
                T2P5Number.TextColor = Color.Red;
                anErrorOccured = true;
            }
            if (T2P6Number.Text == T2P2Number.Text || T2P6Number.Text == T2P3Number.Text
                || T2P6Number.Text == T2P4Number.Text || T2P6Number.Text == T2P5Number.Text
                || T2CaptainNumber.Text == T2P6Number.Text)
            {
                T2P6Number.TextColor = Color.Red;
                anErrorOccured = true;
            }

            // CheckEachPlayer
            if (IsPlayerConfigCorrect(T1CaptainNumber) == false)
                anErrorOccured = true;
            if (IsPlayerConfigCorrect(T1P2Number) == false)
                anErrorOccured = true;
            if (IsPlayerConfigCorrect(T1P3Number) == false)
                anErrorOccured = true;
            if (IsPlayerConfigCorrect(T1P4Number) == false)
                anErrorOccured = true;
            if (IsPlayerConfigCorrect(T1P5Number) == false)
                anErrorOccured = true;
            if (IsPlayerConfigCorrect(T1P6Number) == false)
                anErrorOccured = true;
            if (IsPlayerConfigCorrect(T2CaptainNumber) == false)
                anErrorOccured = true;
            if (IsPlayerConfigCorrect(T2P2Number) == false)
                anErrorOccured = true;
            if (IsPlayerConfigCorrect(T2P3Number) == false)
                anErrorOccured = true;
            if (IsPlayerConfigCorrect(T2P4Number) == false)
                anErrorOccured = true;
            if (IsPlayerConfigCorrect(T2P5Number) == false)
                anErrorOccured = true;
            if (IsPlayerConfigCorrect(T2P6Number) == false)
                anErrorOccured = true;
            return !anErrorOccured;
        }
    }
}