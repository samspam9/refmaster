﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Plugin.Connectivity;
using RefMaster.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RefMaster.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class HistoryPage : ContentPage
    {
        private ObservableCollection<MatchData> hist = new ObservableCollection<MatchData>();
        private List<MatchData> games;

        public HistoryPage()
        {
            InitializeComponent();
            FetchHistory();
            CrossConnectivity.Current.ConnectivityChanged += async (sender, args) =>
            {
                if (!args.IsConnected)
                {
                    await DisplayAlert("Error", "No internet connection!", "Ok");
                    await Navigation.PopToRootAsync();
                }
            };
            HistoryList.ItemsSource = hist;
        }

        // Return Home Button
        private void Home_Clicked(object sender, EventArgs e)
        {
            Navigation.PopToRootAsync();
        }

        // Go Back one page
        private void Back_Clicked(object sender, EventArgs e)
        {
            Navigation.PopAsync(true);
        }

        // Show page for Match details
        void HistoryList_ItemTapped(System.Object sender, Xamarin.Forms.ItemTappedEventArgs e)
        {
            // Why not in a V2
        }

        // Delete this match from the remote server
        void MenuItem_Clicked(System.Object sender, System.EventArgs e)
        {
            MenuItem menuItem = sender as MenuItem;
            var match = menuItem.BindingContext as MatchData;
            var request = new HttpRequestMessage();

            request.Method = HttpMethod.Delete;
            request.RequestUri = new Uri("https://refmaster.uc.r.appspot.com/history/" + match.id);
            try
            {
                httpClient.SendAsync(request);
            } catch (Exception) { }
            hist.Remove(match);
        }

        private static HttpClient httpClient = new HttpClient();

        private async void FetchHistory()
        {
            if (CrossConnectivity.Current.IsConnected == false)
                return;
            var request = new HttpRequestMessage();

            BlockInput(Color.Transparent);
            request.Method = HttpMethod.Get;
            request.RequestUri = new Uri("https://refmaster.uc.r.appspot.com/history");
            try
            {
                HttpResponseMessage resp = await httpClient.SendAsync(request);
                games = JsonConvert.DeserializeObject<List<MatchData>>(await resp.Content.ReadAsStringAsync(), new JsonSerializerSettings{ NullValueHandling = NullValueHandling.Include, Formatting = Formatting.Indented, TypeNameHandling = TypeNameHandling.Auto });
                hist.Clear();
                foreach (MatchData game in games)
                {
                    game.team1.teamColor.color = Color.FromHex(game.team1.teamColor.Value);
                    game.team2.teamColor.color = Color.FromHex(game.team2.teamColor.Value);
                    hist.Add(game);
                }
                UnblockInput();
            }
            catch (Exception)
            {
                await DisplayAlert("Error", "An error occured while fetching the data from the server", "Retry");
                await Navigation.PopToRootAsync();
            }
        }

        private void BlockInput(Color col, string message = "")
        {
            HistoryList.IsVisible = false;
            StatusStack.IsVisible = true;
            StatusLabel.Text = message;
            StatusLabel.TextColor = col;
        }

        private void UnblockInput()
        {
            HistoryList.IsVisible = true;
            StatusStack.IsVisible = false;
        }

        void ContentPage_Appearing(System.Object sender, System.EventArgs e)
        {
            if (CrossConnectivity.Current.IsConnected == false)
            {
                DisplayAlert("Error", "No internet connection!", "Ok");
                Navigation.PopToRootAsync();
            }
        }
    }
}