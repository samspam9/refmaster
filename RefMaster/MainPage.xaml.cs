﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace RefMaster
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }

        // Create a Match
        private void Button_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new Pages.MatchCompoPage());
        }

        // Match History
        private void Button_Clicked_1(object sender, EventArgs e)
        {
            Navigation.PushAsync(new Pages.HistoryPage());
        }
    }
}
